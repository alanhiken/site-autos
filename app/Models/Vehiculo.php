<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'categoria_id', 'llantas', 'potencia',
    ];

    public function categoria()
    {
        return $this->hasOne(Categoria::class, 'id','categoria_id');
    }
}
