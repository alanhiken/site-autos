<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriaController extends Controller
{
    //
      public function listar (Request $request) {
			return $this->_listar(new Categoria(), $request->all());
		}
		
		public function ver (Request $request) {
			return $this->_ver(new Categoria(), $request->all());
		}
		
		public function agregar(Request $request) {
			return $this->_agregar(new Categoria(), $request->all());
		}
		
    public function actualizar(Request $request ) {
			return $this->_actualizar(new Categoria(), $request->all());
		}
        public function eliminar(Request $request ) {
			return $this->_eliminar(new Categoria(), $request->all());
		}
		
	public function paginar(Request $request) {
			$pagineConfig = [
				'valids_sort'    => ['id','estado', 'municipio', 'precio_renta', 'precio_venta'],
				'columns_search' => ['id','estado', 'municipio', 'precio_renta', 'precio_venta'],
				'per_page'       => 15,
				'columns'        => ['*'],
				'model'          => Categoria::class
			];
			return $this->_paginar(
				$pagineConfig,
        $request->all()
			);
	}
	
}
