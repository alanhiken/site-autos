<?php

namespace App\Http\Controllers;

use App\Models\Vehiculo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VehiculosController extends Controller
{
    //
      public function listar (Request $request) {
			return $this->_listar(new Vehiculo(), $request->all());
		}
		
		public function ver (Request $request) {
			return $this->_ver(new Vehiculo(), $request->all());
		}
		
		public function agregar(Request $request) {
			//$archivo = $this->moverArchivo($request->data_img);
			return $this->_agregar(new Vehiculo(), $request->all());
		}
		
    	public function actualizar(Request $request ) {
			return $this->_actualizar(new Vehiculo(), $request->all());
		}
		
		public function eliminar(Request $request ) {
			return $this->_eliminar(new Vehiculo(), $request->all());
		}
		
	public function paginar(Request $request) {
			$pagineConfig = [
				'valids_sort'    => ['id','estado', 'municipio', 'precio_renta', 'precio_venta'],
				'columns_search' => ['id','estado', 'municipio', 'precio_renta', 'precio_venta'],
				'per_page'       => 15,
				'columns'        => ['*'],
				'model'          => Vehiculo::class
			];
			return $this->_paginar(
				$pagineConfig,
        $request->all()
			);
	}

	
}
