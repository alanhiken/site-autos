<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Router\ApiTools;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//C:\laragon\www\site-autos\app\Http\Controllers\LoginController.php
Route::post('/login', 'App\Http\Controllers\LoginController@login');
Route::post('/logout', 'App\Http\Controllers\LoginController@logout');
//Route::post('/register', 'App\Http\Controllers\LoginController@register');

//rutas autentificadas por sanctum 
Route::middleware(['auth:sanctum'])->group(function () {
  Route::get('/user', function (Request $request) {
    return $request->user();
  });
  
  ApiTools::restMethods("vehiculo", "App\Http\Controllers\VehiculosController", []);
  ApiTools::restMethods("categoria", "App\Http\Controllers\CategoriaController", []);
  
  
});
/*Route::get('/listar/autos', function () {
    return ['starbuilks','asda',12];
});

*/