<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categorias')->insert([
            'nombre' => 'Motocicletas',
            'descripcion' => 'Vehiculos de dos ruedas ',
        ]);
        DB::table('categorias')->insert([
            'nombre' => 'Sedan',
            'descripcion' => 'Vehiculos de cuatro ruedas',
        ]);
    }
}
