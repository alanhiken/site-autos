<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'email' => 'soporte@gilasw.com',
            'password' => Hash::make('123456'),
            'rol' => 'admin',
            'nombre' => 'gilasw',
            'a_paterno' => 'admin',
            'a_materno' => 'admin',
        ]);

        DB::table('users')->insert([
            'email' => 'cliente@gilasw.com',
            'password' => Hash::make('123456'),
            'rol' => 'cliente',
            'nombre' => 'gilasw cliente',
            'a_paterno' => 'cliente',
            'a_materno' => 'cliente',
        ]);
    }
}
